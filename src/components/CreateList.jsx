import React, { useState } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import { TextField, Button } from "@mui/material";

const CreateList = ({ onCreateList }) => {
  const [newListName, setNewListName] = useState("");
  const { boardId } = useParams();
  const apiKey = "c52d74467145e7039b2e3a7b3de41375";
  const apiToken =
    "ATTA538771ac8970accdccc1dcb93aff1d3b0ef21a3d8616a15f9bc30bed2cf9e392E3AC6979";

  const handleCreateList = () => {
    axios
      .post(
        `https://api.trello.com/1/boards/${boardId}/lists/?key=${apiKey}&token=${apiToken}`,
        {
          name: newListName,
        }
      )
      .then((response) => {
        setNewListName("");
        onCreateList(response.data);
      })
      .catch((error) => {
        console.error("Error creating list", error);
      });
  };

  return (
    <>
      <TextField
        style={{ paddingBottom: "1rem" }}
        label="New List"
        variant="outlined"
        value={newListName}
        onChange={(e) => setNewListName(e.target.value)}
      />
      <Button variant="contained" onClick={handleCreateList}>
        Create List
      </Button>
    </>
  );
};

export default CreateList;
