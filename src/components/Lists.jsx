import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { Container, CardContent, Grid, Card, Typography } from "@mui/material";
import CreateList from "./CreateList";
import CreateCard from "./CreateCard";
import DeleteList from "./DeleteList";
import DeleteCard from "./DeleteCard";
import CheckList from "./CheckList";
import CreateChecklist from "./CreateCheckList";

const Lists = () => {
  const [lists, setLists] = useState([]);
  const [selectedCardId, setselectedCardId] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { boardId } = useParams();

  const apiKey = "c52d74467145e7039b2e3a7b3de41375";
  const apiToken =
    "ATTA538771ac8970accdccc1dcb93aff1d3b0ef21a3d8616a15f9bc30bed2cf9e392E3AC6979";
  const boardUrl = `https://api.trello.com/1/boards/${boardId}?key=${apiKey}&token=${apiToken}&lists=open&cards=open`;

  useEffect(() => {
    axios
      .get(boardUrl)
      .then((response) => {
        const listsData = response.data.lists.map((list) => ({
          ...list,
          cards: response.data.cards.filter((card) => card.idList === list.id),
        }));

        setLists(listsData);
      })
      .catch((error) => {
        console.error("Error fetching board data", error);
      });
  }, [boardId]);

  const addNewList = (newList) => {
    const newListWithCards = {
      ...newList,
      cards: [],
    };
    setLists([...lists, newListWithCards]);
  };

  const deleteList = (deletedListId) => {
    const updatedLists = lists.filter((list) => list.id !== deletedListId);
    setLists(updatedLists);
  };

  const createCard = (listId, newCard) => {
    const updatedLists = lists.map((list) => {
      if (list.id === listId) {
        return {
          ...list,
          cards: [...list.cards, newCard],
        };
      }
      return list;
    });
    setLists(updatedLists);
  };

  const deleteCard = (listId, cardId) => {
    const updatedLists = lists.map((list) => {
      if (list.id === listId) {
        return {
          ...list,
          cards: list.cards.filter((card) => card.id !== cardId),
        };
      }
      return list;
    });
    setLists(updatedLists);
  };

  const openModal = (cardId) => {
    setselectedCardId(cardId);
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setselectedCardId(null);
    setIsModalOpen(false);
  };

  const onCreateChecklist = (newChecklist) => {
    const updatedLists = lists.map((list) => {
      if (list.id === selectedCardId) {
        return {
          ...list,
          cards: list.cards.map((card) => {
            if (card.id === selectedCardId) {
              return {
                ...card,
                checklists: [...card.checklists, newChecklist],
              };
            }
            return card;
          }),
        };
      }
      return list;
    });
    setLists(updatedLists);
  };

  const listStyle = {
    minHeight: "10rem",
    maxWidth: "20rem",
  };

  const listStyle1 = {
    display: "flex",
    justifyContent: "space-between",
    padding: "1rem",
  };

  const cardStyle = {
    minHeight: "3rem",
    maxWidth: "20rem",
    padding: "1rem",
    marginTop: "1rem",
    display: "flex",
    justifyContent: "space-between",
  };

  return (
    <Container>
      <Typography variant="h4" padding={2}>
        Lists
      </Typography>
      <Grid container>
        {lists.map((list) => (
          <Grid item xs={12} md={4} key={list.id}>
            <Card sx={listStyle}>
              <CardContent>
                <Card sx={listStyle1}>
                  <Typography variant="h5">{list.name}</Typography>
                  <DeleteList listId={list.id} onDeleteList={deleteList} />
                </Card>
                {list.cards.map((card) => (
                  <Card
                    sx={cardStyle}
                    key={card.id}
                    onClick={() => openModal(card.id)}
                  >
                    {card.name}
                    <DeleteCard
                      cardId={card.id}
                      onDeleteCard={(cardId) => deleteCard(list.id, cardId)}
                    />
                  </Card>
                ))}
                <CreateCard listId={list.id} onCreateCard={createCard} />
              </CardContent>
            </Card>
          </Grid>
        ))}
        <Grid item xs={12} md={4} gap={2} padding={2}>
          <Card sx={listStyle}>
            <CardContent>
              <CreateList onCreateList={addNewList} />
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      {isModalOpen && (
        <CheckList cardId={selectedCardId} onClose={closeModal}>
          <CreateChecklist
            cardId={selectedCardId}
            onCreateChecklist={onCreateChecklist}
          />
        </CheckList>
      )}
    </Container>
  );
};

export default Lists;
