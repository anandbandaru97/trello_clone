import { Button } from "@mui/material";
import axios from "axios";

const DeleteCard = ({ cardId, onDeleteCard }) => {
  const handleDelete = () => {
    const apiKey = "c52d74467145e7039b2e3a7b3de41375";
    const apiToken =
      "ATTA538771ac8970accdccc1dcb93aff1d3b0ef21a3d8616a15f9bc30bed2cf9e392E3AC6979";

    axios
      .delete(
        `https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${apiToken}`
      )
      .then(() => {
        onDeleteCard(cardId);
      })
      .catch((error) => {
        console.error("Error deleting card:", error);
      });
  };

  return (
    <Button
      variant="contained"
      color="error"
      onClick={handleDelete}
      className="delete-Button"
    >
      Delete
    </Button>
  );
};

export default DeleteCard;
