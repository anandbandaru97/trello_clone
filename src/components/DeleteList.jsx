import React from "react";
import axios from "axios";
import { Button } from "@mui/material";

const DeleteList = ({ listId, onDeleteList }) => {
  const apiKey = "c52d74467145e7039b2e3a7b3de41375";
  const apiToken =
    "ATTA538771ac8970accdccc1dcb93aff1d3b0ef21a3d8616a15f9bc30bed2cf9e392E3AC6979";

  const handleDeleteList = () => {
    axios
      .put(
        `https://api.trello.com/1/lists/${listId}/closed?key=${apiKey}&token=${apiToken}`,
        {
          value: true,
        }
      )
      .then(() => {
        onDeleteList(listId);
      })
      .catch((error) => {
        console.error("Error deleting list", error);
      });
  };

  return (
      <Button onClick={handleDeleteList} color="error" variant="contained">
        Delete
      </Button>
  );
};

export default DeleteList;
