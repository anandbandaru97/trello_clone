import React from "react";
import axios from "axios";
import { Button } from "@mui/material";

const DeleteCheckListItem = ({ checklistId, checkItemId, onDeleteItem }) => {
  const handleDeleteCheckItem = () => {
    const apiKey = "c52d74467145e7039b2e3a7b3de41375";
    const apiToken =
      "ATTA538771ac8970accdccc1dcb93aff1d3b0ef21a3d8616a15f9bc30bed2cf9e392E3AC6979";

    axios
      .delete(
        `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkItemId}?key=${apiKey}&token=${apiToken}`
      )
      .then((response) => {
        onDeleteItem(checkItemId);
      })
      .catch((error) => {
        console.error("Error deleting check item:", error);
      });
  };

  const buttonStyle = {
    float: "right",
  }

  return (
    <Button variant="contained" color="error" onClick={handleDeleteCheckItem} sx={buttonStyle}>
      DELETE
    </Button>
  );
};

export default DeleteCheckListItem;
