import React from "react";
import { Link } from "react-router-dom";
import TrelloIcon from "/src/assets/trello.svg";

const Header = () => {
  return (
    <header style={{backgroundColor: "#fafafa", padding:"1rem"}}>
      <Link to="/">
      <img src={TrelloIcon} />
      </Link>
    </header>
  );
};

export default Header;
