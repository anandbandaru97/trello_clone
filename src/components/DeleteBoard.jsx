import React, { useState } from "react";
import axios from "axios";
import { Button, Card, Typography } from "@mui/material";

const apiKey = "c52d74467145e7039b2e3a7b3de41375";
const apiToken =
  "ATTA538771ac8970accdccc1dcb93aff1d3b0ef21a3d8616a15f9bc30bed2cf9e392E3AC6979";

const DeleteBoard = ({ boards, onDelete }) => {
  const [deleteBoard, setDeleteBoard] = useState("");
  const [selectedBoardId, setSelectedBoardId] = useState("");

  const handleDeleteBoard = () => {
    if (selectedBoardId.trim() === "") {
      console.warn("Please select a board");
      return;
    }

    axios
      .delete(
        `https://api.trello.com/1/boards/${selectedBoardId}?key=${apiKey}&token=${apiToken}`,
        {
          name: deleteBoard,
        }
      )
      .then((response) => {
        console.log("Deleted Board Data:", selectedBoardId);
        setDeleteBoard("");
        setSelectedBoardId("");
        onDelete(selectedBoardId);
      })
      .catch((error) => {
        console.error("Error", error);
      });
  };

  return (
    <Card className="board-container">
      <Typography variant="h5">Delete a Board</Typography>
      <select
        value={selectedBoardId}
        onChange={(e) => setSelectedBoardId(e.target.value)}
      >
        <option value="">Select a board</option>
        {boards.map((board) => (
          <option key={board.id} value={board.id}>
            {board.name}
          </option>
        ))}
      </select>
      <Button onClick={handleDeleteBoard}>Delete Board</Button>
    </Card>
  );
};

export default DeleteBoard;
