import React, { useState } from "react";
import axios from "axios";
import { TextField, Button } from "@mui/material";

const CreateCard = ({ listId, onCreateCard }) => {
  const [newCardName, setNewCardName] = useState("");
  const apiKey = "c52d74467145e7039b2e3a7b3de41375";
  const apiToken =
    "ATTA538771ac8970accdccc1dcb93aff1d3b0ef21a3d8616a15f9bc30bed2cf9e392E3AC6979";

  const handleAddCard = () => {
    axios
      .post(`https://api.trello.com/1/cards?key=${apiKey}&token=${apiToken}`, {
        name: newCardName,
        idList: listId,
      })
      .then((response) => {
        setNewCardName("");
        onCreateCard(listId, response.data);
      })
      .catch((error) => {
        console.error("Error adding card", error);
      });
  };

  return (
    <>
      <TextField
        style={{ marginTop: "1rem" , paddingBottom: "1rem"}}
        label="New Card"
        variant="outlined"
        value={newCardName}
        onChange={(e) => setNewCardName(e.target.value)}
      />
      <Button variant="contained" onClick={handleAddCard}>
        Add Card
      </Button>
    </>
  );
};

export default CreateCard;
