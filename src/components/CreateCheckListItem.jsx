import React, { useState } from "react";
import axios from "axios";
import { TextField, Button, Box } from "@mui/material";

const CreateCheckListItem = ({ checklistId, onCreateChecklistItem }) => {
  const [itemName, setItemName] = useState("");

  const handleCreateChecklistItem = () => {
    const apiKey = "c52d74467145e7039b2e3a7b3de41375";
    const apiToken = "ATTA538771ac8970accdccc1dcb93aff1d3b0ef21a3d8616a15f9bc30bed2cf9e392E3AC6979";

    axios
      .post(
        `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${apiKey}&token=${apiToken}`,
        {
          name: itemName,
        }
      )
      .then((response) => {
        const newItem = response.data;
        onCreateChecklistItem(newItem);
        setItemName("");
      })
      .catch((error) => {
        console.error("Error creating checklist item:", error);
      });
  };

  return (
    <>
    <Box mt={2}>
      <TextField
        label="Checklist Item Name"
        variant="outlined"
        value={itemName}
        onChange={(e) => setItemName(e.target.value)}
      />
      </Box>
      <Box mt={2}>
        <Button
          variant="contained"
          color="primary"
          onClick={handleCreateChecklistItem}
        >
          Create Checklist Item
        </Button>
      </Box>
    </>
  );
};

export default CreateCheckListItem;
