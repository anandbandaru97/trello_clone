import React from "react";
import axios from "axios";
import { Button } from "@mui/material";

const DeleteChecklist = ({ cardId, checklistId, onDeleteChecklist }) => {
  const apiKey = "c52d74467145e7039b2e3a7b3de41375";
  const apiToken =
    "ATTA538771ac8970accdccc1dcb93aff1d3b0ef21a3d8616a15f9bc30bed2cf9e392E3AC6979";

  const handleDeleteChecklist = () => {
    axios
      .delete(
        `https://api.trello.com/1/cards/${cardId}/checklists/${checklistId}?key=${apiKey}&token=${apiToken}`
      )
      .then(() => {
        onDeleteChecklist(checklistId);
      })
      .catch((error) => {
        console.error("Error deleting checklist:", error);
      });
  };

  return (
    <Button variant="contained" color="error" onClick={handleDeleteChecklist} style={{float: "right"}}>
      Delete Checklist
    </Button>
  );
};

export default DeleteChecklist;
