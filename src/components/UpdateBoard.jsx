import React, { useState } from "react";
import axios from "axios";
import { Button, Card, TextField } from "@mui/material";

const apiKey = "c52d74467145e7039b2e3a7b3de41375";
const apiToken =
  "ATTA538771ac8970accdccc1dcb93aff1d3b0ef21a3d8616a15f9bc30bed2cf9e392E3AC6979";

const UpdateBoard = ({ boards, onUpdate }) => {
  const [updateBoardName, setUpdateBoardName] = useState("");
  const [selectedBoardId, setSelectedBoardId] = useState("");

  const handleUpdateBoard = () => {
    if (selectedBoardId.trim() === "") {
      console.warn("Please select a board");
      return;
    }

    if (updateBoardName.trim() === "") {
      console.warn("Board name cannot be empty");
      return;
    }

    axios
      .put(
        `https://api.trello.com/1/boards/${selectedBoardId}?key=${apiKey}&token=${apiToken}`,
        {
          name: updateBoardName,
        }
      )
      .then((response) => {
        console.log("Updated Board Data:", response.data);
        setUpdateBoardName("");
        setSelectedBoardId("");
        onUpdate(response.data);
      })
      .catch((error) => {
        console.error("Error", error);
      });
  };

  return (
    <Card className="board-container">
      <h1>Update a Board</h1>
      <select
        value={selectedBoardId}
        onChange={(e) => setSelectedBoardId(e.target.value)}
      >
        <option value="">Select a board</option>
        {boards.map((board) => (
          <option key={board.id} value={board.id}>
            {board.name}
          </option>
        ))}
      </select>
      <TextField
        type="text"
        placeholder="Enter the new board name"
        value={updateBoardName}
        onChange={(e) => setUpdateBoardName(e.target.value)}
      />
      <Button onClick={handleUpdateBoard}>Update Board</Button>
    </Card>
  );
};

export default UpdateBoard;
