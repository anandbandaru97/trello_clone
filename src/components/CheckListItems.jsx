import React, { useState, useEffect } from "react";
import axios from "axios";
import { Card, CardContent, Typography } from "@mui/material";
import DeleteCheckListItem from "./DeleteCheckListItem";
import UpdateCheckListItem from "./UpdateCheckListItem";

const CheckListItems = ({ cardId, checklistId, onDeleteItem }) => {
  const [checklistItems, setChecklistItems] = useState([]);

  useEffect(() => {
    const apiKey = "c52d74467145e7039b2e3a7b3de41375";
    const apiToken =
      "ATTA538771ac8970accdccc1dcb93aff1d3b0ef21a3d8616a15f9bc30bed2cf9e392E3AC6979";

    axios
      .get(
        `https://api.trello.com/1/checklist/${checklistId}/checkItems?key=${apiKey}&token=${apiToken}`
      )
      .then((response) => {
        setChecklistItems(response.data);
      })
      .catch((error) => {
        console.error("Error fetching checklist items:", error);
      });
  }, [checklistId]);

  const checkListItem = {
    minHeight: "3rem",
    width: "15rem",
  };

  return (
    <Card sx={checkListItem}>
      <CardContent>
        {checklistItems.map((item) => (
          <Card key={item.id}>
            <UpdateCheckListItem
              cardId={cardId}
              checklistId={checklistId}
              checkItemId={item.id}
            />
            {item.name}
            <DeleteCheckListItem
              checklistId={checklistId}
              checkItemId={item.id}
              onDeleteItem={onDeleteItem}
            />
          </Card>
        ))}
      </CardContent>
    </Card>
  );
};

export default CheckListItems;
