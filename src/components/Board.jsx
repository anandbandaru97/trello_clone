import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { Container, Typography, Grid, Card, CardContent } from "@mui/material";
import CreateBoard from "./CreateBoard";
import UpdateBoard from "./UpdateBoard";
import DeleteBoard from "./DeleteBoard";

const apiKey = "c52d74467145e7039b2e3a7b3de41375";
const apiToken =
  "ATTA538771ac8970accdccc1dcb93aff1d3b0ef21a3d8616a15f9bc30bed2cf9e392E3AC6979";
const apiUrl = `https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${apiToken}`;

const Board = () => {
  const [boards, setBoards] = useState([]);

  useEffect(() => {
    axios
      .get(apiUrl)
      .then((response) => {
        setBoards(response.data);
      })
      .catch((error) => {
        console.error("Error", error);
      });
  }, []);

  const addNewBoard = (newBoard) => {
    setBoards([...boards, newBoard]);
  };

  const updateBoard = (updatedBoard) => {
    const updatedBoards = boards.map((board) =>
      board.id === updatedBoard.id ? updatedBoard : board
    );
    setBoards(updatedBoards);
  };

  const deleteBoard = (deletedBoardId) => {
    const updatedBoards = boards.filter((board) => board.id !== deletedBoardId);
    setBoards(updatedBoards);
  };

  const cardStyle = {
    backgroundImage:
      'url("https://img.freepik.com/premium-photo/serene-snowy-lake-with-misty-mountains-tall-pine-trees_899449-32053.jpg?w=2000")',
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    minHeight: "10rem",
    width: "15rem",
    color: "white",
  };

  const boardStyle = {
    minHeight: "10rem",
    width: "15rem",
  };

  return (
    <Container padding={2}>
      <Typography variant="h4" padding={2}>
        Boards
      </Typography>
      <Grid container gap={5}>
        {boards.map((board) => (
          <Link
            to={`/boards/${board.id}`}
            key={board.id}
            style={{ textDecoration: "none" }}
          >
            <Grid item xs={12} md={4} key={board.id}>
              <Card sx={cardStyle}>
                <CardContent>
                  <Typography variant="h6">{board.name}</Typography>
                </CardContent>
              </Card>
            </Grid>
          </Link>
        ))}
        <Card sx={boardStyle}>
          <CreateBoard addNewBoard={addNewBoard} />
        </Card>
        <Card sx={boardStyle}>
          <UpdateBoard boards={boards} onUpdate={updateBoard} />
        </Card>
        <Card sx={boardStyle}>
          <DeleteBoard boards={boards} onDelete={deleteBoard} />
        </Card>
      </Grid>
    </Container>
  );
};

export default Board;
