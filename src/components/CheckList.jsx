import React, { useState, useEffect } from "react";
import axios from "axios";
import { Card, CardContent, Typography, Modal, Grid } from "@mui/material";
import CreateChecklist from "./CreateCheckList";
import DeleteChecklist from "./DeleteCheckList";
import CheckListItems from "./CheckListItems";
import CreateCheckListItem from "./CreateCheckListItem";

const CheckList = ({ cardId, cardName, onClose }) => {
  const [checklists, setChecklists] = useState([]);
  const [selectedCheckList, setSelectedCheckList] = useState(null);

  useEffect(() => {
    const apiKey = "c52d74467145e7039b2e3a7b3de41375";
    const apiToken =
      "ATTA538771ac8970accdccc1dcb93aff1d3b0ef21a3d8616a15f9bc30bed2cf9e392E3AC6979";
    axios
      .get(
        `https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${apiToken}`
      )
      .then((response) => {
        setChecklists(response.data);
      })
      .catch((error) => {
        console.error("Error fetching checklists:", error);
      });
  }, [cardId]);

  const modalStyle = {
    padding: "1rem",
    maxWidth: "50rem",
  };

  const checkListStyle = {
    display: "flex",
    minHeight: "50rem",
    maxWidth: "50rem",
    padding: "1rem",
  };

  const checkListStyle1 = {
    minHeight: "2rem",
    maxWidth: "20rem",
    padding: ".2rem",
  };

  const createCheckList = {
    height: "8rem",
  };

  const handleSelectChecklist = (checklist) => {
    setSelectedCheckList(checklist);
  };

  const handleDeleteChecklist = (deletedChecklistId) => {
    const updateChecklists = checklists.filter(
      (checklist) => checklist.id !== deletedChecklistId
    );
    setChecklists(updateChecklists);
    if (selectedCheckList?.id === deletedChecklistId) {
      setSelectedCheckList(null);
    }
  };

  const handleCreateChecklistItem = (newItem) => {
    if (selectedCheckList) {
      const updatedChecklist = { ...selectedCheckList };
      if (!updatedChecklist.items) {
        updatedChecklist.items = [];
      }
      updatedChecklist.items.push(newItem);
      const selectedChecklistIndex = checklists.findIndex(
        (checklist) => checklist.id === selectedCheckList.id
      );
      if (selectedChecklistIndex !== -1) {
        const updatedChecklists = [...checklists];
        updatedChecklists[selectedChecklistIndex] = updatedChecklist;
        setChecklists(updatedChecklists);
      }
    }
  };

  const handleDeleteItem = (checkItemId) => {
    const updatedChecklists = [...checklists];
    const selectedChecklist = updatedChecklists.find(
      (checklist) => checklist.id === selectedCheckList.id
    );
    if (selectedChecklist) {
      selectedChecklist.items = selectedChecklist.items.filter(
        (item) => item.id !== checkItemId
      );
      setChecklists(updatedChecklists);
    }
  };

  return (
    <Modal open={true} onClose={onClose} sx={modalStyle}>
      <Card>
        <Typography variant="h5">{cardName}</Typography>
        <Card sx={checkListStyle}>
          {checklists.map((checklist) => (
            <Grid key={checklist.id}>
              <Card
                sx={checkListStyle1}
                key={checklist.id}
                onClick={() => handleSelectChecklist(checklist)}
              >
                <DeleteChecklist
                  cardId={cardId}
                  checklistId={checklist.id}
                  onDeleteChecklist={handleDeleteChecklist}
                />
                <CardContent>
                  {checklist.name}
                  <CheckListItems
                    cardId={cardId}
                    checklistId={checklist.id}
                    checklistItems={checklist.items}
                    selectedCheckList={selectedCheckList}
                    onDeleteItem={handleDeleteItem}
                  />
                  <CreateCheckListItem
                    checklistId={checklist.id}
                    onCreateChecklistItem={handleCreateChecklistItem}
                  />
                </CardContent>
              </Card>
            </Grid>
          ))}
          <Card sx={createCheckList}>
            <CreateChecklist
              cardId={cardId}
              onCreateChecklist={(newChecklist) => {
                setChecklists([...checklists, newChecklist]);
              }}
              onClose={onClose}
            />
          </Card>
        </Card>
      </Card>
    </Modal>
  );
};

export default CheckList;
