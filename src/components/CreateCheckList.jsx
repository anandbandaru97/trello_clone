import React, { useState } from "react";
import axios from "axios";
import { TextField, Button, Box } from "@mui/material";

const CreateChecklist = ({ cardId, onCreateChecklist}) => {
  const [checklistName, setChecklistName] = useState("");

  const handleCreateChecklist = () => {
    // if (!checklistName) {
    //   alert("Please enter a checklist name.");
    //   return;
    // }
    const apiKey = "c52d74467145e7039b2e3a7b3de41375";
    const apiToken =
      "ATTA538771ac8970accdccc1dcb93aff1d3b0ef21a3d8616a15f9bc30bed2cf9e392E3AC6979";

    axios
      .post(
        `https://api.trello.com/1/checklists?key=${apiKey}&token=${apiToken}`,
        {
          name: checklistName,
          idCard: cardId,
        }
      )
      .then((response) => {
        const newChecklist = response.data;
        onCreateChecklist(newChecklist);
        setChecklistName("");
      })
      .catch((error) => {
        console.error("Error creating checklist:", error);
      });
  };

  return (
    <>
      <TextField
        label="Checklist Name"
        variant="outlined"
        value={checklistName}
        onChange={(e) => setChecklistName(e.target.value)}
      />
      <Box mt={2}>
        <Button
          variant="contained"
          color="primary"
          onClick={handleCreateChecklist}
        >
          Create Checklist
        </Button>
      </Box>
    </>
  );
};

export default CreateChecklist;
