import React, { useState } from "react";
import axios from "axios";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";

const UpdateCheckListItem = ({ cardId, checklistId, checkItemId }) => {
  const [isChecked, setIsChecked] = useState(false);

  const apiKey = "c52d74467145e7039b2e3a7b3de41375";
  const apiToken =
    "ATTA538771ac8970accdccc1dcb93aff1d3b0ef21a3d8616a15f9bc30bed2cf9e392E3AC6979";

  const handleUpdateCheckItem = () => {
    const url = `https://api.trello.com/1/cards/${cardId}/checklist/${checklistId}/checkItem/${checkItemId}?key=${apiKey}&token=${apiToken}`;

    const data = {
      state: isChecked ? "incomplete" : "complete",
    };

    axios
      .put(url, data)
      .then(() => {
        setIsChecked(!isChecked);
      })
      .catch((error) => {
        console.error("Error updating check item:", error);
      });
  };

  return (
    <div onClick={handleUpdateCheckItem}>
      {isChecked ? <CheckBoxIcon /> : <CheckBoxOutlineBlankIcon />}
    </div>
  );
};

export default UpdateCheckListItem;
