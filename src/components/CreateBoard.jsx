import React, { useState } from "react";
import axios from "axios";
import { Button, Card, TextField, Typography } from "@mui/material";

const apiKey = "c52d74467145e7039b2e3a7b3de41375";
const apiToken =
  "ATTA538771ac8970accdccc1dcb93aff1d3b0ef21a3d8616a15f9bc30bed2cf9e392E3AC6979";

const CreateBoard = ({ addNewBoard }) => {
  const [newBoardName, setNewBoardName] = useState("");

  const handleCreateBoard = () => {
    if (newBoardName.trim() === "") {
      console.warn("Board name cannot be empty");
      return;
    }

    axios
      .post(`https://api.trello.com/1/boards?key=${apiKey}&token=${apiToken}`, {
        name: newBoardName,
      })
      .then((response) => {
        console.log("New Board Data:", response.data);
        setNewBoardName("");
        addNewBoard(response.data);
      })
      .catch((error) => {
        console.error("Error", error);
      });
  };

  return (
    <Card className="board-container">
      <Typography variant="h5">Create a Board</Typography>
      <TextField
        type="text"
        placeholder="Enter a new board name"
        value={newBoardName}
        onChange={(e) => setNewBoardName(e.target.value)}
      />
      <Button onClick={handleCreateBoard}>Create Board</Button>
    </Card>
  );
};

export default CreateBoard;
