import Board from "./components/Board";
import Header from "./components/Header";
import Lists from "./components/Lists";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";

function App() {
  return (
    <>
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" element={<Board />} />
          <Route path="/boards/:boardId" element={<Lists />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
